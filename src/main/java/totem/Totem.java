package totem;
import java.util.Set;

import tranca.Tranca;


public class Totem {
    public int id;
    public String localizacao;
    public Set<Tranca> trancas;

    public Totem(int id, String localizacao) {
        this.id = id;
        this.localizacao = localizacao;
    }

    public void addTranca(Tranca tranca) {
        trancas.add(tranca);
    }

    public Set<Tranca> getTrancas() {
        return this.trancas;
    }

    public boolean deleteTranca(Tranca tranca) {
        return this.trancas.remove(tranca);
    }
}