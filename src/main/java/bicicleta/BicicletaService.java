package bicicleta;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import io.javalin.http.Context;

// This is a service, it should be independent from Javalin
public class BicicletaService {

    private static Map<Integer, Bicicleta> bicicleta = new HashMap<>();
    private static AtomicInteger lastId;

    static {
        bicicleta.put(0, new Bicicleta(0, 10, "Caloi", "Alice", "2020", Status.DISPONÍVEL));
        bicicleta.put(1, new Bicicleta(1, 10232, "CaloiFake", "AM", "2019", Status.EM_USO));
        bicicleta.put(2, new Bicicleta(2, 1032, "Caloi", "OldFashion", "2000", Status.EM_REPARO));
        lastId = new AtomicInteger(bicicleta.size());
    }

    public static void save(int numero, String marca, String modelo, String ano, Status status) {
        int id = lastId.incrementAndGet();
        bicicleta.put(id , new Bicicleta(id, numero, marca, modelo, ano, status));
    }

    public static Collection<Bicicleta> getAll() {
        return bicicleta.values();
    }

    public static void update(int bicicletaId, int numero, String marca, String modelo, String ano, Status status) {
    	bicicleta.put(bicicletaId, new Bicicleta(bicicletaId, numero, marca, modelo, ano, status));
    }

    public static Bicicleta findById(int idBicicleta) {
        return bicicleta.get(idBicicleta);
    }

    public static void delete(int idBicicleta) {
    	bicicleta.remove(idBicicleta);
    }

	public static void update(Context ctx, Status status) {
        return;
		
	}

}