package tranca;

public class NewTrancaRequest {
	public String bicicleta;
	public int numero;
	public String localizacao;
    public String anoDeFabricacao;
    public String modelo;
    public Status status;

    public NewTrancaRequest() {
    }
    	 
    public NewTrancaRequest(String bicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, Status status) {
    	this.bicicleta = bicicleta;
    	this.numero = numero;
    	this.localizacao = localizacao;
    	this.anoDeFabricacao = anoDeFabricacao;
        this.modelo = modelo;
        this.status = status;
    }
}