package tranca;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import io.javalin.http.Context;

// This is a service, it should be independent from Javalin
public class TrancaService {

    private static Map<Integer, Tranca> tranca = new HashMap<>();
    private static AtomicInteger lastId;

    static {
        tranca.put(0, new Tranca(0, "bicicleta",10, "localizacao", "2020", "modelo", Status.LIVRE));
       // users.put(1, new Tranca(1, "Bob", "bob@bob.java"));
      //  users.put(2, new Tranca(2, "Carol", "carol@carol.java"));
      //  users.put(3, new Tranca(3, "Dave", "dave@dave.java"));
        lastId = new AtomicInteger(tranca.size());
    }

    public static void save(String bicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, Status status) {
        int id = lastId.incrementAndGet();
        tranca.put(id , new Tranca(id, bicicleta, numero, localizacao, anoDeFabricacao, modelo, status));
    }

    public static Collection<Tranca> getAll() {
        return tranca.values();
    }

    public static void update(int trancaId, String bicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, Status status) {
    	tranca.put(trancaId, new Tranca(trancaId, bicicleta, numero, localizacao, anoDeFabricacao, modelo, status));
    }

    public static Tranca findById(int trancaId) {
        return tranca.get(trancaId);
    }

    public static void delete(int trancaId) {
    	tranca.remove(trancaId);
    }

	public static void update(Context ctx, Status status) {
		// TODO Auto-generated method stub
		
	}

}